# yii2-extended/yii2-crud

A library that provides crud (i.e. low level backend admin) controllers and actions for relational models.

![coverage](https://gitlab.com/yii2-extended/yii2-crud-html/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-extended/yii2-crud-html/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-extended/yii2-crud ^8`


## Configuration

This module needs the following components to be set at the configuration level:

- NONE


This module uses the following parameters to be set at the configuration level:

- NONE


Then the module should be configured as follows (in `console.php` or `web.php`) :

```php
$config = [
	...
	'modules' => [
		...
		'crud' => [
			'class' => 'Yii2Extended\Yii2Crud\CrudModule',
			'layout' => '//layout.php',
			'bundles' => [
				'url-module-id' => 'module-id-in-this-config-file',
			],
		],
		...
	],
	...
];
```


## License

MIT (See [license file](LICENSE))

<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-crud library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Crud\Components;

use yii\base\Action;
use yii\filters\AccessRule;
use yii\web\Request;
use yii\web\User;

/**
 * RbacAccessRule class file.
 * 
 * This access rules is to make permissions checks relatives to the requested
 * module/bundle/record in the crud model.
 * 
 * @author Anastaszor
 * @psalm-suppress PropertyNotSetInConstructor
 */
class RbacAccessRule extends AccessRule
{
	
	/**
	 * The template to create a request-based permission. The template may
	 * contain the following placeholders :
	 * - {action} will be replaced with the action id
	 * - {module} will be replaced with the wanted module id
	 * - {bundle} will be replaced with the wanted bundle id
	 * - {record} will be replaced with the wanted record id.
	 * 
	 * @var string
	 */
	public string $permTemplate = '';
	
	/**
	 * {@inheritDoc}
	 * @see \yii\filters\AccessRule::allows()
	 * @param Action $action
	 * @param User|false $user
	 * @param Request $request
	 * @return null|boolean
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function allows($action, $user, $request)
	{
		$moduleId = $request->get('moduleId');
		if(!\is_string($moduleId) || empty($moduleId) || !\preg_match('#^[a-z0-9-]+$#', $moduleId))
		{
			return false;
		}
		
		$bundleId = $request->get('bundleId');
		if(!\is_string($bundleId) || empty($bundleId) || !\preg_match('#^[a-z0-9-]+$#', $bundleId))
		{
			return false;
		}
		
		$recordId = $request->get('recordId');
		if(!\is_string($recordId) || empty($recordId) || !\preg_match('#^[a-z0-9-]+$#', $recordId))
		{
			return false;
		}
		
		$this->permissions = [\strtr($this->permTemplate, [
			'{action}' => $action->id,
			'{module}' => $moduleId,
			'{bundle}' => $bundleId,
			'{record}' => $recordId,
		])];
		
		return parent::allows($action, $user, $request);
	}
	
}

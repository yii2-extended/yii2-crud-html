<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-crud library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Crud\Components;

use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Stringable;
use yii\BaseYii;
use yii\rbac\ManagerInterface;
use yii\rbac\Permission;
use yii\rbac\Role;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;

/**
 * RbacStructure class file.
 * 
 * This represents the rbac structure to be initialized or repaired for this
 * module. This will check for the controllers of this module as well as the
 * delegated crud items.
 * 
 * @author Anastaszor
 */
class RbacStructure implements Stringable
{
	
	public const ROLE_READONLY = 'readonly';
	public const ROLE_MODIFIER = 'modifier';
	public const ROLE_MANAGER = 'manager';
	
	public const PERM_INDEX = 'index';
	public const PERM_SEARCH = 'search';
	public const PERM_VIEW = 'view';
	public const PERM_CREATE = 'create';
	public const PERM_UPDATE = 'update';
	public const PERM_DELETE = 'delete';
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new RbacStructure with the given logger.
	 * 
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the auth manager.
	 * 
	 * @return ManagerInterface
	 * @throws RuntimeException
	 */
	public function getAuthManager() : ManagerInterface
	{
		$auth = BaseYii::$app->authManager;
		if(null === $auth)
		{
			$message = 'Failed to find auth manager from \\Yii::$app';
			
			throw new RuntimeException($message);
		}
		
		return $auth;
	}
	
	/**
	 * Ensure that all the default items are in place.
	 * 
	 * @param ModuleInterface $module
	 * @return array{'readonly': Role, 'modifier': Role, 'manager': Role}
	 * @throws \yii\base\Exception
	 * @throws Exception
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function createAllPermissions(ModuleInterface $module) : array
	{
		$auth = $this->getAuthManager();
		
		$allReadOnlyName = 'crud|all|'.self::ROLE_READONLY;
		$allReadOnly = $auth->createRole($allReadOnlyName);
		$allReadOnly->description = 'CRUD All Modules Read-Only';
		if(!$auth->getRole($allReadOnlyName))
		{
			$auth->add($allReadOnly);
		}
		
		$allModifierName = 'crud|all|'.self::ROLE_MODIFIER;
		$allModifier = $auth->createRole($allModifierName);
		$allModifier->description = 'CRUD All Modules Modifier';
		if(!$auth->getRole($allModifierName))
		{
			$auth->add($allModifier);
		}
		if(!$auth->hasChild($allModifier, $allReadOnly))
		{
			$auth->addChild($allModifier, $allReadOnly);
		}
		
		$allManagerName = 'crud|all|'.self::ROLE_MANAGER;
		$allManager = $auth->createRole($allManagerName);
		$allManager->description = 'CRUD All Modules Manager';
		if(!$auth->getRole($allManagerName))
		{
			$auth->add($allManager);
		}
		if(!$auth->hasChild($allManager, $allModifier))
		{
			$auth->addChild($allManager, $allModifier);
		}
		
		$permissions = $this->createPermissionModule($module);
		if(!$auth->hasChild($allReadOnly, $permissions[self::ROLE_READONLY]))
		{
			$auth->addChild($allReadOnly, $permissions[self::ROLE_READONLY]);
		}
		if(!$auth->hasChild($allModifier, $permissions[self::ROLE_MODIFIER]))
		{
			$auth->addChild($allModifier, $permissions[self::ROLE_MODIFIER]);
		}
		if(!$auth->hasChild($allManager, $permissions[self::ROLE_MANAGER]))
		{
			$auth->addChild($allManager, $permissions[self::ROLE_MANAGER]);
		}
		
		return [
			self::ROLE_READONLY => $allReadOnly,
			self::ROLE_MODIFIER => $allModifier,
			self::ROLE_MANAGER => $allManager,
		];
	}
	
	/**
	 * Creates the roles for the modules in the given module.
	 * 
	 * @param ModuleInterface $module
	 * @return array{'readonly': Role, 'modifier': Role, 'manager': Role}
	 * @throws \yii\base\Exception
	 * @throws Exception
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function createPermissionModule(ModuleInterface $module) : array
	{
		$auth = $this->getAuthManager();
		
		$moduleReadOnlyName = 'crud|module|'.self::ROLE_READONLY.'|'.$module->getId();
		$moduleReadOnly = $auth->createRole($moduleReadOnlyName);
		$moduleReadOnly->description = 'CRUD Module Read-Only for '.$module->getId();
		if(!$auth->getRole($moduleReadOnlyName))
		{
			$auth->add($moduleReadOnly);
		}
		
		$moduleModifierName = 'crud|module|'.self::ROLE_MODIFIER.'|'.$module->getId();
		$moduleModifier = $auth->createRole($moduleModifierName);
		$moduleModifier->description = 'CRUD Module Modifier for '.$module->getId();
		if(!$auth->getRole($moduleModifierName))
		{
			$auth->add($moduleModifier);
		}
		if(!$auth->hasChild($moduleModifier, $moduleReadOnly))
		{
			$auth->addChild($moduleModifier, $moduleReadOnly);
		}
		
		$moduleManagerName = 'crud|module|'.self::ROLE_MANAGER.'|'.$module->getId();
		$moduleManager = $auth->createRole($moduleManagerName);
		$moduleManager->description = 'CRUD Module Manager for '.$module->getId();
		if(!$auth->getRole($moduleManagerName))
		{
			$auth->add($moduleManager);
		}
		if(!$auth->hasChild($moduleManager, $moduleModifier))
		{
			$auth->addChild($moduleManager, $moduleModifier);
		}
		
		foreach($module->getBundles() as $bundle)
		{
			$permissions = $this->createPermissionBundle($module, $bundle);
			if(!$auth->hasChild($moduleReadOnly, $permissions[self::ROLE_READONLY]))
			{
				$auth->addChild($moduleReadOnly, $permissions[self::ROLE_READONLY]);
			}
			if(!$auth->hasChild($moduleModifier, $permissions[self::ROLE_MODIFIER]))
			{
				$auth->addChild($moduleModifier, $permissions[self::ROLE_MODIFIER]);
			}
			if(!$auth->hasChild($moduleManager, $permissions[self::ROLE_MANAGER]))
			{
				$auth->addChild($moduleManager, $permissions[self::ROLE_MANAGER]);
			}
		}
		
		return [
			self::ROLE_READONLY => $moduleReadOnly,
			self::ROLE_MODIFIER => $moduleModifier,
			self::ROLE_MANAGER => $moduleManager,
		];
	}
	
	/**
	 * Creates the base roles for all bundles in the given module.
	 * 
	 * @param ModuleInterface $module
	 * @param BundleInterface $bundle
	 * @return array{'readonly': Role, 'modifier': Role, 'manager': Role}
	 * @throws \yii\base\Exception
	 * @throws Exception
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function createPermissionBundle(ModuleInterface $module, BundleInterface $bundle) : array
	{
		$auth = $this->getAuthManager();
		
		$bundleReadOnlyName = 'crud|bundle|'.self::ROLE_READONLY.'|'.$module->getId().'|'.$bundle->getId();
		$bundleReadOnly = $auth->createRole($bundleReadOnlyName);
		$bundleReadOnly->description = 'CRUD Bundle Read-Only for '.$module->getId().'|'.$bundle->getId();
		if(!$auth->getRole($bundleReadOnlyName))
		{
			$auth->add($bundleReadOnly);
		}
		
		$bundleModifierName = 'crud|bundle|'.self::ROLE_MODIFIER.'|'.$module->getId().'|'.$bundle->getId();
		$bundleModifier = $auth->createRole($bundleModifierName);
		$bundleModifier->description = 'CRUD Bundle Modifier for '.$module->getId().'|'.$bundle->getId();
		if(!$auth->getRole($bundleModifierName))
		{
			$auth->add($bundleModifier);
		}
		if(!$auth->hasChild($bundleModifier, $bundleReadOnly))
		{
			$auth->addChild($bundleModifier, $bundleReadOnly);
		}
		
		$bundleManagerName = 'crud|bundle|'.self::ROLE_MANAGER.'|'.$module->getId().'|'.$bundle->getId();
		$bundleManager = $auth->createRole($bundleManagerName);
		$bundleManager->description = 'CRUD Bundle Manager for '.$module->getId().'|'.$bundle->getId();
		if(!$auth->getRole($bundleManagerName))
		{
			$auth->add($bundleManager);
		}
		if(!$auth->hasChild($bundleManager, $bundleModifier))
		{
			$auth->addChild($bundleManager, $bundleModifier);
		}
		
		foreach($bundle->getEnabledRecords() as $record)
		{
			$permissions = $this->createPermissionsForClass($module, $bundle, $record);
			if(!$auth->hasChild($bundleReadOnly, $permissions[self::ROLE_READONLY]))
			{
				$auth->addChild($bundleReadOnly, $permissions[self::ROLE_READONLY]);
			}
			if(!$auth->hasChild($bundleModifier, $permissions[self::ROLE_MODIFIER]))
			{
				$auth->addChild($bundleModifier, $permissions[self::ROLE_MODIFIER]);
			}
			if(!$auth->hasChild($bundleManager, $permissions[self::ROLE_MANAGER]))
			{
				$auth->addChild($bundleManager, $permissions[self::ROLE_MANAGER]);
			}
		}
		
		return [
			self::ROLE_READONLY => $bundleReadOnly,
			self::ROLE_MODIFIER => $bundleModifier,
			self::ROLE_MANAGER => $bundleManager,
		];
	}
	
	/**
	 * Creates the base roles for all classes in the given module.
	 * 
	 * @param ModuleInterface $module
	 * @param BundleInterface $bundle
	 * @param RecordInterface $record
	 * @return array{'readonly': Role, 'modifier': Role, 'manager': Role}
	 * @throws \yii\base\Exception
	 * @throws Exception
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function createPermissionsForClass(ModuleInterface $module, BundleInterface $bundle, RecordInterface $record) : array
	{
		$auth = $this->getAuthManager();
		
		$indexPermission = $this->createPermissionsForClassAction($module, $bundle, $record, self::PERM_INDEX);
		$searchPermission = $this->createPermissionsForClassAction($module, $bundle, $record, self::PERM_SEARCH);
		$viewPermission = $this->createPermissionsForClassAction($module, $bundle, $record, self::PERM_VIEW);
		
		$roleReadOnlyName = 'crud|class|'.self::ROLE_READONLY.'|'.$module->getId().'|'.$bundle->getId().'|'.$record->getId();
		$roleReadOnly = $auth->createRole($roleReadOnlyName);
		$roleReadOnly->description = 'CRUD Role Read-Only for '.$module->getId().'|'.$bundle->getId().'|'.$record->getClass();
		if(!$auth->getRole($roleReadOnlyName))
		{
			$auth->add($roleReadOnly);
		}
		if(!$auth->hasChild($roleReadOnly, $indexPermission))
		{
			$auth->addChild($roleReadOnly, $indexPermission);
		}
		if(!$auth->hasChild($roleReadOnly, $searchPermission))
		{
			$auth->addChild($roleReadOnly, $searchPermission);
		}
		if(!$auth->hasChild($roleReadOnly, $viewPermission))
		{
			$auth->addChild($roleReadOnly, $viewPermission);
		}
		
		$createPermission = $this->createPermissionsForClassAction($module, $bundle, $record, self::PERM_CREATE);
		$updatePermission = $this->createPermissionsForClassAction($module, $bundle, $record, self::PERM_UPDATE);
		
		$roleModifierName = 'crud|class|'.self::ROLE_MODIFIER.'|'.$module->getId().'|'.$bundle->getId().'|'.$record->getId();
		$roleModifier = $auth->createRole($roleModifierName);
		$roleModifier->description = 'CRUD Role Modifier for '.$module->getId().'|'.$bundle->getId().'|'.$record->getClass();
		if(!$auth->getRole($roleModifierName))
		{
			$auth->add($roleModifier);
		}
		if(!$auth->hasChild($roleModifier, $roleReadOnly))
		{
			$auth->addChild($roleModifier, $roleReadOnly);
		}
		if(!$auth->hasChild($roleModifier, $createPermission))
		{
			$auth->addChild($roleModifier, $createPermission);
		}
		if(!$auth->hasChild($roleModifier, $updatePermission))
		{
			$auth->addChild($roleModifier, $updatePermission);
		}
		
		$deletePermission = $this->createPermissionsForClassAction($module, $bundle, $record, self::PERM_DELETE);
		
		$roleManagerName = 'crud|class|'.self::ROLE_MANAGER.'|'.$module->getId().'|'.$bundle->getId().'|'.$record->getId();
		$roleManager = $auth->createRole($roleManagerName);
		$roleManager->description = 'CRUD Role Manager for '.$module->getId().'|'.$bundle->getId().'|'.$record->getClass();
		if(!$auth->getRole($roleManagerName))
		{
			$auth->add($roleManager);
		}
		if(!$auth->hasChild($roleManager, $roleModifier))
		{
			$auth->addChild($roleManager, $roleModifier);
		}
		if(!$auth->hasChild($roleManager, $deletePermission))
		{
			$auth->addChild($roleManager, $deletePermission);
		}
		
		return [
			self::ROLE_READONLY => $roleReadOnly,
			self::ROLE_MODIFIER => $roleModifier,
			self::ROLE_MANAGER => $roleManager,
		];
	}
	
	/**
	 * Creates the permission for the given record class and action.
	 * 
	 * @param ModuleInterface $module
	 * @param BundleInterface $bundle
	 * @param RecordInterface $record
	 * @param string $action the action name
	 * @return Permission
	 * @throws Exception
	 */
	public function createPermissionsForClassAction(ModuleInterface $module, BundleInterface $bundle, RecordInterface $record, string $action) : Permission
	{
		$auth = $this->getAuthManager();
		$permissionName = 'crud|class|'.$action.'|'.$module->getId().'|'.$bundle->getId().'|'.$record->getId();
		$permission = $auth->createPermission($permissionName);
		$permission->description = 'CRUD '.\ucfirst($action).' permission for '.$module->getId().'|'.$bundle->getId().'|'.$record->getClass();
		if(!$auth->getPermission($permissionName))
		{
			$auth->add($permission);
		}
		
		return $permission;
	}
	
}

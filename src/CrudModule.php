<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-crud library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Crud;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Module\Helper\BootstrappedModule;

/**
 * CrudModule class file.
 * 
 * This module gathers all the configuration generated at runtime for the
 * other modules that are to be crud-able.
 * 
 * @author Anastaszor
 */
class CrudModule extends BootstrappedModule
{
	
	/**
	 * The modules that are to be used.
	 * url id of the module => id of the module in the module's application.
	 * 
	 * @var array<string, string>
	 */
	public array $moduleIds = [];
	
	/**
	 * The bundles that are verified and available.
	 * 
	 * @var array<string, ModuleInterface&Module>
	 */
	protected array $_verifiedModules = [];
	
	/**
	 * Gets the bundle that implements the bundle interface.
	 * 
	 * @param string $moduleUri
	 * @return null|(Module&ModuleInterface)
	 */
	public function getBundle(string $moduleUri) : ?ModuleInterface
	{
		$moduleId = $this->moduleIds[$moduleUri] ?? null;
		if(null === $moduleId)
		{
			return null;
		}
		
		$module = BaseYii::$app->getModule($moduleId);
		if(null === $module)
		{
			$message = 'The object as {name} is not a valid module (null)';
			$context = ['name' => $moduleUri];
			
			BaseYii::warning(BaseYii::t('CrudModule.Module', $message, $context));
			
			return null;
		}
		
		if(!$module instanceof ModuleInterface)
		{
			$message = 'The object as {name} is not an instance of ModuleInterface but a {object}';
			/** @phpstan-ignore-next-line */ /** @psalm-suppress DocblockTypeContradiction */
			$context = ['name' => $moduleUri, 'object' => \gettype($module) === 'object' ? \get_class($module) : \gettype($module)];
			
			BaseYii::warning(BaseYii::t('CrudModule.Module', $message, $context));
			
			return null;
		}
		
		return $module;
	}
	
	/**
	 * Gets the bundles that are correctly installed on this module.
	 *
	 * @return array<string, ModuleInterface&Module>
	 */
	public function getVerifiedModules() : array
	{
		if([] === $this->_verifiedModules)
		{
			$this->_verifiedModules = [];
			
			foreach($this->moduleIds as $modulePath => $moduleUri)
			{
				$bundle = $this->getBundle($modulePath);
				/** @phpstan-ignore-next-line */
				if(null !== $bundle && $bundle instanceof Module && $bundle instanceof ModuleInterface)
				{
					$this->_verifiedModules[$modulePath] = $bundle;
					
					continue;
				}
				
				$message = 'Failed to find bundle {path} => {name}';
				$context = ['path' => $modulePath, 'name' => $moduleUri];
				
				BaseYii::warning(BaseYii::t('CrudModule.Module', $message, $context));
			}
		}
		
		return $this->_verifiedModules;
	}
	
}

<?php declare(strict_types=1);

use yii\BaseYii;
use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;
use yii\db\ActiveRecord;
use yii\web\View;

/** @var View $this */
/** @var ActiveRecord $model */
/** @var ActiveForm $form */
/** @author Anastaszor */
?>
<div class="record-form col-10 offset-1">

<?php $form = ActiveForm::begin();

foreach($model->attributes() as $attributeName)
{
	// remove all the meta_ fields that are automatically filled by the database
	if(\mb_strpos($attributeName, 'meta_') === 0)
	{
		continue;
	}
	
	echo $form->field($model, $attributeName)->textInput(['maxlength' => true]);
}

?>
<div class="form-group">
	<?php echo Html::submitButton(BaseYii::t('CrudModule.View', 'Save'), ['class' => 'btn btn-success']); ?>
</div>

<?php ActiveForm::end(); ?>

</div>

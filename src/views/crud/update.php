<?php declare(strict_types=1);

use yii\BaseYii;
use yii\bootstrap5\Html;
use yii\db\ActiveRecordInterface;
use yii\web\View;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;

/** @var View $this */
/** @var array<integer, ModuleInterface> $modules */
/** @var ModuleInterface $module */
/** @var BundleInterface $bundle */
/** @var RecordInterface $record */
/** @var ActiveRecordInterface $model */
/** @author Anastaszor */
$this->beginContent(__DIR__.'/../layouts/layout.php', [
	'modules' => $modules,
	'module' => $module,
	'bundle' => $bundle,
	'record' => $record,
	'model' => $model,
]); ?>

<span class="btn-group float-end"><?php echo Html::a(
	BaseYii::t('CrudModule.View', 'View'),
	['crud/view', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()] + (array) $model->getPrimaryKey(true),
	['class' => 'btn btn-primary'],
); 
echo Html::a(
	BaseYii::t('CrudModule.View', 'Delete'),
	['crud/delete', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()] + (array) $model->getPrimaryKey(true),
	['class' => 'btn btn-danger', 'data-confirm' => BaseYii::t('yii', 'Are you sure you want to delete this item?')],
);
?></span>
<h1 class="col-10 offset-1"><?php 
/** @psalm-suppress MixedArgumentTypeCoercion */
echo Html::encode(((string) $record->getLabel()).' '.\implode(', ', (array) ($model->getPrimaryKey(true))));
?></h1>
<?php

echo $this->render('_form', [
	'model' => $model,
]);

$this->endContent();

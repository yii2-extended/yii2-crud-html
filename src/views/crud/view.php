<?php declare(strict_types=1);

use yii\base\Model;
use yii\BaseYii;
use yii\bootstrap5\Html;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use yii\helpers\ArrayHelper;
use yii\i18n\Formatter;
use yii\web\View;
use yii\widgets\DetailView;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;

/** @var View $this */
/** @var array<integer, ModuleInterface> $modules */
/** @var ModuleInterface $module */
/** @var BundleInterface $bundle */
/** @var RecordInterface $record */
/** @var ActiveRecordInterface $model */
/** @author Anastaszor */
$this->beginContent(__DIR__.'/../layouts/layout.php', [
	'modules' => $modules,
	'module' => $module,
	'bundle' => $bundle,
	'record' => $record,
	'model' => $model,
]); ?>

<span class="btn-group float-end"><?php echo Html::a(
	BaseYii::t('CrudModule.View', 'Update'),
	['update', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()] + (array) $model->getPrimaryKey(true),
	['class' => 'btn btn-primary'],
); 
echo Html::a(
	BaseYii::t('CrudModule.View', 'Delete'),
	['delete', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()] + (array) $model->getPrimaryKey(true),
	['class' => 'btn btn-danger', 'data-confirm' => BaseYii::t('yii', 'Are you sure you want to delete this item?')],
);
?></span>
<h1><?php 
/** @psalm-suppress MixedArgumentTypeCoercion */
echo Html::encode(((string) $record->getLabel()).' '.\implode(', ', (array) ($model->getPrimaryKey(true))));
?></h1>
<?php

$attributes = [];

foreach($model->attributes() as $attrName)
{
	$attributes[] = [
		'attribute' => $attrName,
		'format' => 'raw',
		'value' => function(Model $model, DetailView $widget) use ($modules, $attrName) : string
		{
			/** @var ?class-string<ActiveRecord> $relationClass */
			$relationClass = null;
			/** @var ?ActiveRecord $relation */
			$relation = null;
			
			$rclass = new ReflectionClass($model);

			/** @var ReflectionMethod $rmethod */
			foreach($rclass->getMethods(ReflectionMethod::IS_PUBLIC) as $rmethod)
			{
				if(\mb_substr($rmethod->getName(), 0, 3) !== 'get')
				{
				continue;
				}
				if($rmethod->getNumberOfParameters() !== 0)
				{
				continue;
				}
				
				$return = $rmethod->invoke($model);
				if($return instanceof ActiveQuery)
				{
					// only the direct links [foreign key => primary key]
					if(\count($return->link) === 1 && \array_keys($return->link)[0] === $attrName)
					{
						$relationClass = $return->modelClass;
						$nrelation = $return->one();
						if($nrelation instanceof ActiveRecord)
						{
							$relation = $nrelation;
						}
						
						break;
					}
				}
			}
			
			$rendered = Html::encode((string) ArrayHelper::getValue($model, $attrName));
			
			if(null !== $relationClass && null !== $relation)
			{
				/** @var ModuleInterface $module */
				foreach($modules as $module)
				{
					/** @var BundleInterface $bundle */
					foreach($module->getBundles() as $bundle)
					{
						/** @var RecordInterface $record */
						foreach($bundle->getEnabledRecords() as $record)
						{
							if($record->getClass() === $relationClass && $record->isAllowed(RecordInterface::ACTION_VIEW))
							{
								$rendered = Html::a($rendered, [
									'crud/view',
									'moduleId' => $module->getId(),
									'bundleId' => $bundle->getId(),
									'recordId' => $record->getId(),
								] + (array) ($relation->getPrimaryKey(true)));
								
								break 3;
							}
						}
					}
				}
			}
			
			if(!empty($rendered))
			{
				return $rendered;
			}
			
			$formatter = $widget->formatter;
			if($formatter instanceof Formatter)
			{
				return (string) $formatter->nullDisplay;
			}
			
			return '<span class="text-muted">(not set)</span>';
		},
		'captionOptions' => [
			'class' => 'col-4',
		],
		'contentOptions' => [
			'class' => 'col-',
		],
	];
}

echo DetailView::widget([
	'model' => $model,
	'attributes' => $attributes,
	'formatter' => [
		'class' => Formatter::class,
		'nullDisplay' => '<span class="text-muted">'.Html::encode(BaseYii::t('yii', '(not set)')).'</span>',
	],
]);

$this->endContent();

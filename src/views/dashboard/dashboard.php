<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-crud-html library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use yii\BaseYii;
use yii\bootstrap5\Html;
use yii\web\View;

/** @var View $this */
/** @var array<integer, ModuleInterface> $modules */
/** @var ?ModuleInterface $module */
/** @var ?BundleInterface $bundle */
/** @var ?RecordInterface $record */
/** @var ?ActiveRecordInterface $model */
/** @author Anastaszor */
$this->beginContent(__DIR__.'/../layouts/layout.php', [
	'modules' => $modules,
	'module' => $module,
	'bundle' => $bundle,
	'record' => $record,
	'model' => $model,
]);

$nombreModules = \count($modules);

?>

<div class="row">
	<div class="col-md-3 col-sm-6">
		<div class="card text-dark bg-warning mb-3">
			<div class="card-body">
				<h5 class="card-title"><?php echo Html::encode(BaseYii::t('CrudModule.View', 'Nombre de Modules')); ?></h5>
				<p class="card-text fs-1"><?php echo Html::encode((string) $nombreModules); ?></p>
			</div>
		</div>
	</div>
</div>

<?php $this->endContent();
